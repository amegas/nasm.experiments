#!/bin/bash

chmod +x ./scripts/clean.sh && ./scripts/clean.sh

echo -e "\nRunning the NASM assembler for building the object file with the debug symbols:"
nasm -g -f elf64 -l main.lst main.asm -o main.o
file ./main.o

echo -e "\nLinking the object file:"
ld main.o -o main
file ./main

echo -e "\nMaking the copy of the output executable file for the strip process:"
cp main main.stripped
strip ./main.stripped
file  ./main.stripped

echo -e "\nDisassembling the the output executable files:"
objdump -D ./main > dis.main
objdump -D ./main.stripped > dis.main.stripped
file ./dis.main
file ./dis.main.stripped

echo -e "\nShowing the size (in bytes) of the built files:"
wc -c ./main.o
wc -c ./main.lst
wc -c ./main
wc -c ./main.stripped
wc -c ./dis.main
wc -c ./dis.main.stripped

echo -e "\nGenerating the hexdumps..."
hexdump ./main > hex.main
hexdump ./main.stripped > hex.main.stripped