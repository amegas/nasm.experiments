#!/bin/bash

echo -e "Cleaning the built files..."
rm -f main.o main.lst main main.stripped dis.main dis.main.stripped hex.main hex.main.stripped

if [ "$1" == "remove-symlinks" ];
then
    rm clean compile run
fi