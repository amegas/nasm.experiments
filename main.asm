%include 'functions.asm'

SECTION .data
msgFirst     db  "Hello World!"    , 0h
msgSecond    db  "Another string." , 0h
timesToPrint equ 10

SECTION .bss
userInput: resb 255

SECTION .text
global _start

_start:
    pop  rcx

next_argument:
    cmp  rcx, 0h
    jz   no_args
    pop  rax
    mov  rsi, rax
    push rcx
    call sprint_linefeed
    pop  rcx
    dec  rcx
    jmp  next_argument

no_args:
    mov  rcx, timesToPrint

first_message:
    mov  rsi, msgFirst
    push rcx
    call sprint_linefeed
    pop  rcx
    dec  rcx
    jnz  first_message

second_message:
    mov  rsi, msgSecond
    call sprint_linefeed

test_user_input:
    mov  rsi, userInput
    call read_input

call quit