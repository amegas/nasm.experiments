SECTION .data
inputTemplate  db  "User input: ", 0h
maxBytesToRead equ 255

sprint:
    push rdx
    push rdi
    push rax
    push rsi

    mov  rax, rsi
    pop  rsi
    jmp  next_char
    ret

sprint_linefeed:
    call sprint

    push rsi
    mov  rsi, 0Ah
    push rsi
    mov  rsi, rsp

    call sprint
    pop  rsi
    pop  rsi
    ret

read_input:
    push rsi
    call kernel_fn_read_input
    mov  r8 , rsi
    mov  rsi, inputTemplate
    call sprint
    mov  rsi, r8
    call sprint
    pop rsi
    ret

quit:
    call kernel_fn_quit
    ret

next_char:
    cmp byte [rax], 0
    jz  count_length
    inc rax
    jmp next_char

count_length:
    sub rax, rsi
    mov rdx, rax
    jmp kernel_fn_print_string

kernel_fn_print_string:
    mov rax, 1
    mov rdi, 1
    syscall

    pop rax
    pop rdi
    pop rdx
    ret

kernel_fn_read_input:
    push rax
    push rdx
    push rdi

    mov rax, 0
    mov rdx, maxBytesToRead
    mov rdi, 0
    syscall

    pop rdi
    pop rdx
    pop rax
    ret

kernel_fn_quit:
    push rax
    push rdi

    mov rax, 60
    mov rdi, 0
    syscall

    pop rdi
    pop rax
    ret